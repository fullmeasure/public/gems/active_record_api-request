# ActiveRecordApi::Request
This is a library intended to make it easy to access services using the ActiveRecordApi::RestController gem.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'active_record_api-request'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install active_record_api-request
```

## Usage
### Query [index action]
```ruby
ActiveRecordApi::Request::Methods.new(
  host: 'https://advisor.beta.fullmeasureed.com/api', 
  path: 'integrations/integrations', 
  token: 'nfwv89n4wvb98bgb0ver87*******', 
  debug: true
  
).query(params: {orginization_id: 1, type: 'IntegrationS3'})
```

### GET [show action]
```ruby
ActiveRecordApi::Request::Methods.new(
  host: 'https://advisor.beta.fullmeasureed.com/api', 
  path: 'integrations/integrations', 
  token: 'nfwv89n4wvb98bgb0ver87*******', 
  debug: true
  
).get(id: 1)
```

### POST [create action]
```ruby
ActiveRecordApi::Request::Methods.new(
  host: 'https://advisor.beta.fullmeasureed.com/api', 
  path: 'integrations/integrations', 
  token: 'nfwv89n4wvb98bgb0ver87*******', 
  debug: true
).post(payload: {orginization_id: 1, type: 'IntegrationS3'})
```

### PUT [create action]
```ruby
ActiveRecordApi::Request::Methods.new(
  host: 'https://advisor.beta.fullmeasureed.com/api', 
  path: 'integrations/integrations', 
  token: 'nfwv89n4wvb98bgb0ver87a*****', 
  debug: true
).put(id: 1, payload: {orginization_id: 1, type: 'IntegrationS3'})
```
## Authentication
- If you have a token you can just provide it directly
- Alternatively, you can provide a email and password
    - NOTE: Make sure you also provide an environment variable for the token path

```ruby
ActiveRecordApi::Request::Methods.new(
  host: 'https://advisor.beta.fullmeasureed.com/api', 
  path: 'integrations/integrations', 
  email: 'samuel.bi**@gmail.com', 
  password: 'P@ss***', 
).get(id: 1)
```

## Configuration
- Many of the credentials can be provided via environment variables or Rails 5.2+ credentials

### Environment Variables  
```bash
export ACTIVE_RECORD_API_REQUEST_HOST=https://advisor.beta.fullmeasureed.com/api
export ACTIVE_RECORD_API_REQUEST_token=nfwv89n4wvb98bgb0ver87*******
export ACTIVE_RECORD_API_REQUEST_EMAIL=samuel.bi**@gmail.com
export ACTIVE_RECORD_API_REQUEST_PASSWORD=P@ss***
export ACTIVE_RECORD_API_REQUEST_TOKEN_PATH=staff-auth/token
```  
### Rails 5.2+ Credentials
```yaml
active_record_api_request:
    host: https://advisor.beta.fullmeasureed.com/api
    token: nfwv89n4wvb98bgb0ver87*******
    email: samuel.bi**@gmail.com
    password: P@ss***
    token_path: staff-auth/token
```

### GET using environment variables or credentials
```ruby
ActiveRecordApi::Request::Methods.new(
  path: 'integrations/integrations', 
).get(id: 1)
```


## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
