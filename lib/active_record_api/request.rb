require 'active_support'

module ActiveRecordApi
  module Request
    extend ActiveSupport::Autoload

    autoload :VERSION
    autoload :Methods
    autoload :Credentials
    autoload :Connection
    autoload :FaradayFollowNextLinks
    autoload :FaradayCacheServiceDown
    autoload :AuthError
    autoload :FaradayAuthTokenRetry
    autoload :TokenRetriever
    autoload :TokenCache
  end
end
