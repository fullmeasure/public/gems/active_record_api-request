module ActiveRecordApi
  module Request
    class FaradayCacheServiceDown < Faraday::Middleware
      attr_reader :cache
      attr_reader :cache_identifier

      def initialize(app, cache = nil, cache_identifier = 'empty')
        super(app)
        @cache = cache
        @cache_identifier = cache_identifier
      end

      def call(env)
        if :get == env[:method]
          make_get_request(env)
        else
          @app.call(env)
        end
      end

      def create_response(env)
        hash = env.to_hash
        {
          status: hash[:status],
          body: unless hash[:body].nil? then hash[:body] else hash[:response_body] end,
          response_headers: hash[:response_headers]
        }
      end

      def make_get_request(env)
        @app.call(env).on_complete do |response_env|
          cache.write(cache_key(env), create_response(response_env))
          response_env
        end
      rescue Faraday::TimeoutError
        cached_response = cache.read(cache_key(env))
        raise unless cached_response.present?
        env.update(cached_response)
        Faraday::Response.new(env)
      end

      def cache_key(env)
        url = env[:url].dup
        url.normalize!
        "service_down:#{url.request_uri}:#{cache_identifier}"
      end
    end
  end
end
