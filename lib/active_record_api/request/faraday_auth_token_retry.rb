require 'faraday_middleware'
#:nocov:#

module ActiveRecordApi
  module Request
    class FaradayAuthTokenRetry < Faraday::Middleware
      AUTH_KEY = 'Authorization'.freeze

      attr_reader :credentials, :host, :token_path, :debug, :timeout

      def initialize(app, credentials: nil, host: nil, token_path: nil, debug: false, timeout: nil)
        super(app)
        @credentials = credentials
        @host = host
        @token_path = token_path
        @debug = debug
        @timeout = timeout
      end

      def call(env)
        if env.request_headers['Retries'].present?
          env.request_headers['Retries'] = (env.request_headers['Retries'].to_i + 1).to_s
        else
          env.request_headers['Retries'] = '0'
        end
        if env.request_headers['InjectToken'].present? && env.request_headers['InjectToken'] == 'true'
          env.request_headers[AUTH_KEY] = "Bearer #{retrieve_token}"
        end
        @app.call(env)
      rescue Faraday::ClientError => error
        if error.respond_to?(:response) && [401, 403].include?(error.response.try(:[], :status)) && env.request_headers['Retries'] != '5'
          if @debug
            Honeybadger.notify(error, {context: {message: 'faraday error, flushing'}})
          end
          token_cache.flush
          call(env)
        else
          if @debug
            Honeybadger.notify(error, {context: {message: 'faraday error, not flushing'}})
          end
          raise error
        end
      end

      def retrieve_token
        token_cache.fetch_token Proc.new { TokenRetriever.new(credentials: credentials, host: host, token_path: token_path, debug: debug, timeout: timeout).fetch_token }
      end

      def token_cache
        @token_cache ||= TokenCache.new(debug: debug)
      end
    end
  end
end
#:nocov:#
