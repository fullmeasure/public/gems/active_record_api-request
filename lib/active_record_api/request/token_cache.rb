#:nocov:#
module ActiveRecordApi
  module Request
    class TokenCache
      include ActiveAttr::Model
      attr_accessor :debug

      def flush
        return unless defined? AUTH_REDIS_POOL
        debug_log 'flushing token cache'
        AUTH_REDIS_POOL.with do |client|
          @redis_client = client
          @redis_client.del('auth_token')
          release_lock
        end
        debug_log 'token cache has been flushed'
      end

      def fetch_token(token_proc)
        retrieve_token_from_redis(token_proc)
      end

      def retrieve_token_from_redis(token_proc)
        debug_log 'looking for token in cache'
        unless defined? AUTH_REDIS_POOL
          debug_log 'no auth redis pool. getting new token'
          token = token_proc.call
          raise "no token returned! #{token}" if token.nil?
          return token
        end
        AUTH_REDIS_POOL.with do |client|
          @redis_client = client
          @token = nil
          @retry = 0
          while @token.nil? && @retry < 5
            @token = @redis_client.get('auth_token')
            debug_log "retry ##{@retry}"
            debug_log "found token: #{@token}"
            @retry += 1
            if @token.nil? || session_missing_for?(@token)
              if acquire_lock
                begin
                  debug_log 'acquired lock and fetching token for cache'
                  @token = token_proc.call
                  raise "no token returned! #{@token}" if @token.nil?
                  @redis_client.set 'auth_token', @token
                ensure
                  debug_log 'releasing lock'
                  release_lock
                end
              else
                debug_log 'waiting for token to be cached'
                sleep 3
              end
            end
          end
        end
        raise "no token found after waiting #{@retry} times" if @token.nil?
        @token
      end

      def redis_session_impossible?
        return true unless defined? Redis
        ENV['REDIS_SESSION_HOST'].nil? || ENV['REDIS_SESSION'].nil?
      end

      def redis_session
        if redis_session_impossible?
          debug_log 'Redis session environment variables not present.'
          return nil
        end

        @redis_session ||= Redis.new({ url: "redis://#{ENV['REDIS_SESSION_HOST']}", db: ENV['REDIS_SESSION'] })
      end

      def session_missing_for?(token)
        return false if redis_session.nil?
        session = (redis_session.send(:get, token) if redis_session.respond_to?(:get))
        debug_log("Session located for token #{token}") unless session.nil?
        debug_log("Session could not be located for token #{token}") if session.nil?
        session.nil?
      end

      def debug_log(message)
        Rails.logger.info message if debug && defined? Rails
      end

      def acquire_lock
        @redis_client.set 'auth_token_lock', unique_key, nx: true, ex: 8
        current_lock_value = @redis_client.get 'auth_token_lock'
        debug_log "#{current_lock_value} == #{unique_key}"
        current_lock_value == unique_key
      end

      def release_lock
        @redis_client.del 'auth_token_lock'
      end

      def unique_key
        @unique_key ||= rand.to_s
      end
    end
  end
end

#:nocov:#
