#:nocov:#
module ActiveRecordApi
  module Request
    class TokenRetriever < Methods
      attr_accessor :credentials, :host, :token_path

      alias_method :path, :token_path

      def fetch_token
        post(payload: credentials)[:token]
      end

      def headers
        @headers ||= {}
      end
    end
  end
end
#:nocov:#
