require 'faraday_middleware'
#:nocov:#
module ActiveRecordApi
  module Request
    class AuthError < Faraday::ClientError
    end
  end
end
#:nocov:#
