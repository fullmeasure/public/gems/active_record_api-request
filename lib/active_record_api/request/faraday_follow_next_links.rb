require 'faraday_middleware'

module ActiveRecordApi
  module Request
    class FaradayFollowNextLinks < Faraday::Middleware
      ENV_TO_CLEAR = %i[status response response_headers].to_set.freeze

      attr_reader :max_pages

      def initialize(app, max_pages = nil)
        super(app)
        @max_pages = max_pages
      end

      def call(env)
        if :get == env[:method]
          perform_with_next_links(env, max_pages)
        else
          @app.call(env)
        end
      end

      def perform_with_next_links(env, max_pages_left)
        request_body = env[:body]
        response = @app.call(env)
        response.on_complete do |response_env|
          if next_link(env).present?
            raise FaradayMiddleware::RedirectLimitReached, response if max_pages_left == 0
            handle_response(env, request_body, response_env, max_pages_left)
          end
        end
        response
      end

      def next_link(env)
        env.response.headers.fetch('x-link-next', nil)
      end

      def handle_response(env, request_body, response_env, max_pages_left)
        return unless env[:body].is_a?(Array)
        new_request_env = update_env(response_env.dup, request_body, next_link(env))
        max_pages_left -= 1 unless max_pages_left.nil?
        env[:body] += perform_with_next_links(new_request_env, max_pages_left).body
      end

      def update_env(env, request_body, next_link)
        env[:url] = URI.parse(next_link)
        env[:body] = request_body
        ENV_TO_CLEAR.each { |key| env.delete key }
        env
      end
    end
  end
end
