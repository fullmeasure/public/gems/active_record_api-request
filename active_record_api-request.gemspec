$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'active_record_api/request/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'active_record_api-request'
  spec.version     = ActiveRecordApi::Request::VERSION
  spec.authors       = ['Full Measure Education']
  spec.email         = ['devops@fullmeasureed.com']
  spec.homepage    = 'https://gitlab.com/fullmeasure/public/gems/active_record_api-request'
  spec.summary     = 'A ruby library for making requests to the services using the active_record_api-rest_controller gem'
  spec.description = 'A ruby library for making requests to the services using the active_record_api-rest_controller gem'
  spec.license     = 'MIT'

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency 'active_attr'
  spec.add_dependency 'activesupport'
  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday-http-cache'
  spec.add_dependency 'faraday_middleware'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'webmock'
end
