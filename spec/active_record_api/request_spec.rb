require 'spec_helper'

RSpec.describe ActiveRecordApi::Request do
  it "has a version number" do
    expect(ActiveRecordApi::Request::VERSION).not_to be nil
  end
end
