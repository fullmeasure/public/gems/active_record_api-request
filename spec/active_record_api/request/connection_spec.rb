require 'spec_helper'

RSpec.describe ActiveRecordApi::Request::Connection do
  let(:host) { "https://advisor.beta.fullmeasureed.com"}
  let(:token) { SecureRandom.hex }
  let(:service) {  described_class.new(path: '/api/integrations/integrations', host: host, token: token, cache_store: cache) }
  let(:cache) { ActiveSupport::Cache.lookup_store(:memory_store) }
  before(:each) do
    cache.clear
  end

  describe 'http_cache_options' do
    it { expect(service.http_cache_options).to eq({shared_cache: false, store: cache}) }
  end

  describe 'service_url' do
    let(:url_suffix) { '/okcomputer/all.json' }
    it { expect(service.service_url(url_suffix)).to eq "#{host}/api/integrations#{url_suffix}"}
  end
end
