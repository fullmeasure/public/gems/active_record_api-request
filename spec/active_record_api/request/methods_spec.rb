require 'spec_helper'

RSpec.describe ActiveRecordApi::Request::Methods, type: :service do
  let(:host) { "https://advisor.beta.fullmeasureed.com/api/"}
  let(:token) { SecureRandom.hex }
  let(:service) {  described_class.new(path: 'integrations/integrations', host: host, token: token, cache_store: cache) }
  let(:cache) { ActiveSupport::Cache.lookup_store(:memory_store) }
  before(:each) do
    cache.clear
  end
  describe 'query' do
    context 'when the service returns immediately' do
      it 'returns the body of a get service' do
        stub_request(:get, service.full_url_with_params).to_return(body:"{'test':'test'}", status: 200)
        expect(service.query).to eq "{'test':'test'}"
      end
    end
    context 'when the service is delayed' do
      it 'returns the body of a get service' do
        stub_request(:get, service.full_url_with_params).to_return(body:nil, status: 202)
        stub_request(:get, service.full_url_with_params).to_return(body:"{'test':'test'}", status: 200)
        expect(service.query).to eq "{'test':'test'}"
      end
    end
    context 'when it receives an exception' do
      it 'throws an exception' do
        exception = Faraday::ClientError.new({:status => 400, :headers => {}, :body => 'response body'})
        expect(service).to receive(:connection).and_raise(exception)
        expect { service.query }.to raise_exception(Faraday::ClientError)
      end
    end
  end

  describe 'get' do
    context 'when the service returns immediately' do
      it 'returns the body of a get service' do
        stub_request(:get, "#{service.full_url}/1").to_return(body:"{'test':'test'}", status: 200)
        expect(service.get(id: 1)).to eq "{'test':'test'}"
      end
      it 'returns the body of a get service with additional params provided' do
        stub_request(:get, "#{service.full_url}/1?organization_id=1").to_return(body:"{'test':'test'}", status: 200)
        expect(service.get(id: 1, params: { organization_id: 1 })).to eq "{'test':'test'}"
      end
    end

    context 'when the service is delayed' do
      it 'returns the body of a get service' do
        stub_request(:get, "#{service.full_url}/1").to_return(body:nil, status: 202)
        stub_request(:get, "#{service.full_url}/1").to_return(body:"{'test':'test'}", status: 200)
        expect(service.get(id: 1)).to eq "{'test':'test'}"
      end
    end
    context 'when it receives an exception' do
      it 'throws an exception' do
        exception = Faraday::ClientError.new({:status => 400, :headers => {}, :body => 'response body'})
        expect(service).to receive(:connection).and_raise(exception)
        expect { service.get(id: 1) }.to raise_exception(Faraday::ClientError)
      end
    end
  end


  describe 'post' do
    context 'when the service returns immediately' do
      it 'returns the body of a get service' do
        stub_request(:post, service.full_url_with_params).to_return(body:"{'test':'test'}", status: 200)
        expect(service.post(payload: {id:[1,2]})).to eq "{'test':'test'}"
      end
    end
    context 'when it receives an exception' do
      it 'throws an exception' do
        exception = Faraday::ClientError.new({:status => 400, :headers => {}, :body => "{'test':'test'}"})
        expect(service).to receive(:connection).and_raise(exception)
        expect { service.post(payload: {test: 'test'}) }.to raise_exception(Faraday::ClientError)
      end
    end
  end

  describe 'put' do
    context 'when the service returns immediately' do
      it 'returns the body of a get service' do
        stub_request(:put, "#{service.full_url}/1").to_return(body:"{'test':'test'}", status: 200)
        expect(service.put(id: 1, payload: {something:[1,2]}) ).to eq "{'test':'test'}"
      end
    end
    context 'when it receives an exception' do
      it 'throws an exception' do
        exception = Faraday::ClientError.new({:status => 400, :headers => {}, :body => "{'test':'test'}"})
        expect(service).to receive(:connection).and_raise(exception)
        expect { service.put(id: 1, payload: {test: 'test'}) }.to raise_exception(Faraday::ClientError)
      end
    end
  end

  describe 'symbolize_response' do
    context 'when it is a string' do
      it 'returns the response as a string' do
        expect(service.symbolize_response('some string')).to eq 'some string'
      end
    end

    context 'when it is an array' do
      context 'when it has hashes in the array' do
        it 'symbolizes each thing in the array' do
          expect(service.symbolize_response([{'test' => 'something'}])).to eq [{test: 'something'}]
        end
      end
      context 'when it has ints in the array' do
        it 'returns the response as it' do
          expect(service.symbolize_response([24,35])).to eq [24,35]
        end
      end
      context 'when it has strings in the array' do
        it 'returns the response as it' do
          expect(service.symbolize_response(%w(dogs cats))).to eq %w(dogs cats)
        end
      end
    end

    context 'when it is an hash' do
      it 'symbolizes it' do
        expect(service.symbolize_response({'test' => 'something'})).to eq({test: 'something'})
      end
    end
  end

  describe 'service_healthy?' do
    before(:each) do
      stub_request(:get, "#{service.service_url(service.health_endpoint)}").to_return(body: '{"default": {"success": true}}', headers: {content_type: 'application/json'})
    end
    it { expect(service.service_healthy?).to eq true }
  end
end
