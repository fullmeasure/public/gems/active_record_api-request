require 'spec_helper'

RSpec.describe ActiveRecordApi::Request::Credentials do

  describe 'host' do
    before(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_HOST"] = 'https://test.com'
    end

    after(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_HOST"] = nil
    end
    it { expect(subject.host).to eq( ENV["ACTIVE_RECORD_API_REQUEST_HOST"]) }
  end

  describe 'token_path' do
    before(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_TOKEN_PATH"] = 'staff-auth/token'
    end

    after(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_TOKEN_PATH"] = nil
    end
    it { expect(subject.token_path).to eq( ENV["ACTIVE_RECORD_API_REQUEST_TOKEN_PATH"]) }
  end

  describe 'email' do
    before(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"] = 'sam@fullmeasureed.com'
    end

    after(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"] = nil
    end
    it { expect(subject.email).to eq( ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"]) }
  end

  describe 'password' do
    before(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"] = 'P@ssw0rd!'
    end

    after(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"] = nil
    end
    it { expect(subject.password).to eq( ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"]) }
  end

  describe 'credentials' do
    before(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"] = 'sam@fullmeasureed.com'
      ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"] = 'P@ssw0rd!'
    end

    after(:each) do
      ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"] = nil
      ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"] = nil
    end
    it { expect(subject.credentials[:email]).to eq( ENV["ACTIVE_RECORD_API_REQUEST_EMAIL"]) }
    it { expect(subject.credentials[:password]).to eq( ENV["ACTIVE_RECORD_API_REQUEST_PASSWORD"]) }
    it { expect(subject.credentials[:grant_type]).to eq('password') }
  end
end
