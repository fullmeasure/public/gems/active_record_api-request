require 'spec_helper'

RSpec.describe ActiveRecordApi::Request::FaradayFollowNextLinks, type: :service do
  let(:host) { "https://advisor.beta.fullmeasureed.com/api/"}
  let(:token) { SecureRandom.hex }
  let(:service) {  ActiveRecordApi::Request::Methods.new(path: 'integrations/integrations', host: host, token: token, cache_store: cache) }
  let(:cache) { ActiveSupport::Cache.lookup_store(:memory_store) }
  before(:each) do
    cache.clear
  end
  describe 'caching' do
    let!(:stubbed_request) do
      stub_request(:get, "https://advisor.beta.fullmeasureed.com/api/integrations/integrations/").
        with(headers: {'Authorization'=>"Bearer #{token}"}).
        to_return(status: 200, body: [
          {
            field_module_key: 'stuff',
            name: 'name1',
            key: 'some_string',
            data_type: 'string'
          },
          {
            field_module_key: 'stuff',
            name: 'name2',
            key: 'some_date',
            data_type: 'date'
          },
          {
            field_module_key: 'stuff',
            name: 'name3',
            key: 'some_boolean',
            data_type: 'boolean'
          },
          {
            field_module_key: 'stuff',
            name: 'name4',
            key: 'some_integer',
            data_type: 'integer'
          },
        ].to_json, headers: {'x-link-next' => 'http://next-link', content_type: 'application/json'})
    end
    it 'works when intermittently down' do
      stubbed_request = stub_request(:get, service.full_url_with_params).to_return(body:[{test:'test'}].to_json, status: 200, headers: {'x-link-next' => 'http://next-link', content_type: 'application/json'})
      stubbed_next_request = stub_request(:get, "http://next-link/").to_return(body:[{test:'test'}].to_json, status: 200, headers: {'x-link-next' => '', content_type: 'application/json'})
      service.query
      expect(stubbed_request).to have_been_requested.once
      expect(stubbed_next_request).to have_been_requested.once
    end
  end
end
