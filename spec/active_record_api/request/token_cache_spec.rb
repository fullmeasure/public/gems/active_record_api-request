require 'spec_helper'

class RedisStub
  def store
    @store ||= {}
  end

  def get(key)
    store[key]
  end

  def set(key, value, *ignore)
    store[key] = value
  end

  def del(key)
    store.delete(key)
  end
end

class RedisPoolStub
  def redis
    @redis ||= RedisStub.new
  end
  def with(&block)
    block.call(redis)
  end
end

describe 'ActiveRecordApi::Request::TokenCache' do
  it 'should cache token' do
    stub_const("AUTH_REDIS_POOL", RedisPoolStub.new)
    token_cache = ActiveRecordApi::Request::TokenCache.new(debug: true)

    # Uses supplied proc to get and cache token
    token_cache.fetch_token Proc.new { 'abc' }
    expect(AUTH_REDIS_POOL.redis.get('auth_token')).to eq 'abc'

    # Doesn't uses proc again because the token is cached
    token_cache.fetch_token Proc.new { 'xyz' }
    expect(AUTH_REDIS_POOL.redis.get('auth_token')).to eq 'abc'
  end

  it 'should use proc if auth redis pool does not exist' do
    token_cache = ActiveRecordApi::Request::TokenCache.new(debug: true)

    token = token_cache.fetch_token Proc.new { '123' }
    expect(token).to eq '123'
  end
end