require 'bundler/setup'
Bundler.setup
require 'simplecov'
require 'webmock/rspec'
require 'pry'
require 'pry-byebug'
require 'faraday'
require 'faraday_middleware'
require 'active_attr'
SimpleCov.start 'rails'
SimpleCov.minimum_coverage 100
require 'active_record_api/request' # and any other gems you need

SimpleCov.start do
  add_filter "lib/active_record_api/request/version.rb"
end
